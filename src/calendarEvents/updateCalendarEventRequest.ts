import { Length, IsString, IsInt, IsPositive, IsISO8601, IsOptional } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';

export class UpdateCalendarEventRequest {
  @Length(3, 255)
  @IsOptional()
  @IsString()
  @JSONSchema({ description: 'The title of the Calendar Event.' })
  title?: string;

  @Length(3, 1000)
  @IsOptional()
  @IsString()
  @JSONSchema({ description: 'The description of the Calendar Event.' })
  description?: string;

  @IsInt()
  @IsPositive()
  @IsOptional()
  @JSONSchema({ description: 'The Thread ID of the Calendar Event.' })
  threadId?: number;

  @IsISO8601()
  @IsOptional()
  @JSONSchema({ description: 'The start date of the Calendar Event in ISO 8601 format.' })
  startsAt?: string;

  @IsISO8601()
  @IsOptional()
  @JSONSchema({ description: 'The end date of the Calendar Event in ISO 8601 format.' })
  endsAt?: string;
}
