import { IsObject, ValidateNested } from 'class-validator';
import { ThreadWithLastPost } from '../threads/threadWithLastPost';
import { Viewers } from '../threads/viewers';
import { Subforum } from '../subforums/subforum';

export class ThreadSearchResult extends ThreadWithLastPost {
  @ValidateNested()
  subforum: Subforum;

  @IsObject()
  @ValidateNested()
  viewers: Viewers;
}
