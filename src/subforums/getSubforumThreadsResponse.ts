import { Type } from 'class-transformer';
import { IsNumber, ValidateNested } from 'class-validator';
import { ThreadWithLastPost } from '../threads/threadWithLastPost';
import { Subforum } from './subforum';

export class GetSubforumThreadsResponse {
  @ValidateNested()
  subforum: Subforum;

  @IsNumber()
  page: number;

  @ValidateNested({ each: true })
  @Type(() => ThreadWithLastPost)
  threads: ThreadWithLastPost[];
}
