import { IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';
import { Post } from '../posts/post';

export class Subforum {
  @IsNumber()
  id: number;

  @IsString()
  name: string;

  @IsString()
  createdAt: string;

  @IsString()
  updatedAt: string;

  @IsString()
  description: string;

  @IsString()
  icon: string;

  @IsNumber()
  totalThreads: number;

  @IsOptional()
  @IsNumber()
  totalPosts?: number;

  @IsOptional()
  @ValidateNested()
  lastPost?: Post;
}
