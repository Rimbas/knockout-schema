import { IsInt, IsOptional, IsString, ValidateNested } from 'class-validator';
import { UpdateUserProfileRequest } from './updateUserProfileRequest';
import { UserProfileBackground } from './userProfileBackground';

export class UserProfile extends UpdateUserProfileRequest {
  @IsInt()
  id: number;

  @ValidateNested()
  background: UserProfileBackground;

  @IsOptional()
  @IsString()
  header?: string;
}
