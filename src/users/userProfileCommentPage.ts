import { Type } from 'class-transformer';
import { IsInt, ValidateNested } from 'class-validator';
import { UserProfileComment } from './userProfileComment';

export class UserProfileCommentPage {
  @IsInt()
  totalComments: number;

  @IsInt()
  page: number;

  @ValidateNested({ each: true })
  @Type(() => UserProfileComment)
  comments: UserProfileComment[];
}
