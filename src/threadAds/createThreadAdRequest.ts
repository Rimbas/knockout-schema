import { IsString, Length } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';

export class CreateThreadAdRequest {
  @IsString()
  @Length(5, 150)
  @JSONSchema({ description: 'The description of the thread for this Thread Ad' })
  description: string;

  @IsString()
  @Length(5, 200)
  @JSONSchema({
    description:
      'The search query this user will be directed to when the user clicks this Thread Ad',
  })
  query: string;

  @IsString()
  @JSONSchema({ description: 'The filename of the uploaded thread ad image' })
  filename: string;
}