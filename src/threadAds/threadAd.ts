import { IsDateString, IsInt, IsString } from 'class-validator';

export class ThreadAd {
  @IsInt()
  id: number;

  @IsString()
  description: string;

  @IsString()
  query: string;

  @IsString()
  imageUrl: string;

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;
}