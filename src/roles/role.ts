import { IsArray, IsDateString, IsInt, IsOptional, IsString } from 'class-validator';
import { PermissionCode } from '../permissionCode';

export class Role {
  @IsInt()
  id: number;

  @IsString()
  code: string;

  @IsString()
  description: string;

  @IsOptional()
  @IsArray()
  permissionCodes: PermissionCode[];

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;
}
