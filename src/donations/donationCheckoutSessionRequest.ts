import { IsString } from 'class-validator';

export class DonationCheckoutSessionRequest {
  @IsString()
  successUrl: string;

  @IsString()
  cancelUrl: string;
}
