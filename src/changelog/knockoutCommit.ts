import { IsDateString, IsNumber, IsOptional, IsString } from 'class-validator';

// Represents a commit to a Knockout project
export class KnockoutCommit {
  // the commit id
  @IsString()
  id: string;

  // the project name of the commit (knockout-api, knockout-frontend, etc)
  @IsString()
  projectName: string;

  // the commit message
  @IsString()
  title: string;

  // the gitlab author username
  @IsString()
  authorName: string;

  // (optional) the corresponding Knockout userId
  @IsNumber()
  @IsOptional()
  userId?: number;

  // the date of the commit
  @IsDateString()
  date: string;

  // the web url leading to the diff
  @IsString()
  url: string;
}
