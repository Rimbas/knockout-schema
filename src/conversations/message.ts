import { IsDateString, IsInt, IsString, ValidateNested } from "class-validator";
import { User } from "../users/user";

export class Message {
  @IsInt()
  id: number;

  @IsInt()
  conversationId: number;

  @ValidateNested()
  user: User;

  @IsString()
  content: string;

  @IsDateString()
  readAt: string | null;

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;
}
