import { IsDateString, IsOptional, IsString } from 'class-validator';

export class GoldSubscription {
  @IsString()
  status: string;

  @IsString()
  stripeId: string;

  @IsDateString()
  @IsOptional()
  startedAt?: string;

  @IsDateString()
  @IsOptional()
  canceledAt?: string;

  @IsDateString()
  @IsOptional()
  nextPaymentAt?: string;
}
