import {
  ArrayMaxSize,
  IsBoolean,
  IsInt,
  IsObject,
  IsOptional,
  ValidateNested,
} from 'class-validator';
import { Post } from '../posts/post';
import { Thread } from './thread';
import { ReadThread } from './readThread';
import { Rating } from '../posts/rating';
import { Viewers } from './viewers';

export class ThreadWithLastPost extends Thread {
  @IsBoolean()
  deleted: boolean;

  @IsBoolean()
  locked: boolean;

  @IsBoolean()
  pinned: boolean;

  @ArrayMaxSize(3)
  @IsObject({ each: true })
  tags: Object[];

  @IsOptional()
  @ValidateNested()
  readThread?: ReadThread;

  @IsOptional()
  @ValidateNested()
  firstPostTopRating?: Rating;

  @ValidateNested()
  lastPost: Post;

  @IsInt()
  postCount: number;

  @IsObject()
  @ValidateNested()
  viewers: Viewers;
}
