import { IsInt } from 'class-validator';
import { User } from '../users/user';

export class Viewers {
  @IsInt()
  memberCount: number;

  @IsInt()
  guestCount: number;

  users?: User[];
}
