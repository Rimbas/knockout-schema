import { IsBoolean, IsDateString, IsInt, IsOptional, IsString, ValidateNested } from 'class-validator';
import { Post } from '../posts/post';
import { Thread } from '../threads/thread';
import { User } from '../users/user';

export class Ban {
  @IsInt()
  id: number;

  @IsString()
  banReason: string;

  @IsDateString()
  expiresAt: string;

  @ValidateNested()
  user: User;

  @IsOptional()
  @ValidateNested()
  post: Post;

  @IsOptional()
  @ValidateNested()
  thread: Thread;

  @ValidateNested()
  bannedBy: User;

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;

  @IsBoolean()
  reverted: boolean;
}
