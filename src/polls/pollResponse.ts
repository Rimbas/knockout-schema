import { IsDateString, IsNumber } from "class-validator";

export class PollResponse {
  @IsNumber()
  id: number;

  @IsNumber()
  pollOptionId: number;

  @IsNumber()
  userId: number;

  @IsDateString()
  createdAt: string;
}