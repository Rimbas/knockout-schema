export enum PollType {
  CHOOSE_ONE = 'chooseOne',
  CHOOSE_MULTIPLE = 'chooseMultiple'
}